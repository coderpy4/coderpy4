<div align="center">
<img src="greetings.gif" align="center" style="width: 100%" />
</div>  
  
### <div align="center">I'm Andrii, a developer 👨‍💻 studying programming since 2016 🕶️</div>  
  
- 📚 I’m currently studying in school  
  
- 🐧 Fun fact: I use Linux BTW...  

- <img src="undertale.png" align="center" /> I love Undertale. Papirus <3

- <img src="stranger_things.png" width=64 align="center" />  **one love!**

## Connect with me  

<div align="center">
<a href="https://github.com/coderpy4" target="_blank">
<img src=https://img.shields.io/badge/github-%2324292e.svg?&style=for-the-badge&logo=github&logoColor=white alt=github style="margin-bottom: 5px;" />
</a>
<a href="https://fosstodon.org/@coderpy4" target="_blank">
<img src=https://img.shields.io/badge/mastodon-6364FF.svg?&style=for-the-badge&logo=mastodon&logoColor=white alt=mastodon style="margin-bottom: 5px;" />
</a>
<a href="https://instagram.com/coderpy.4" target="_blank">
<img src=https://img.shields.io/badge/instagram-%23000000.svg?&style=for-the-badge&logo=instagram&logoColor=white alt=instagram style="margin-bottom: 5px;" />
</a>
<a href="https://www.youtube.com/user/coderpy4" target="_blank">
<img src=https://img.shields.io/badge/youtube-%23EE4831.svg?&style=for-the-badge&logo=youtube&logoColor=white alt=youtube style="margin-bottom: 5px;" />
</a>  
</div>  
  
<br/>  

## Github Stats  

<div align="center"><img src="https://github-readme-stats.vercel.app/api?username=coderpy4&show_icons=true&count_private=true&hide_border=true" align="center" /></div>  

<br/>  

<div align="center"><img src="https://spotify-github-profile.vercel.app/api/view?uid=18djc6wrp2msr80u35yd4bjzj&cover_image=true&theme=default&show_offline=true&background_color=121212&bar_color=53b14f&bar_color_cover=true" /></div>  

<br/>  

<div align="center">
            <a href="https://www.buymeacoffee.com/coderpy4" target="_blank" style="display: inline-block;">
                <img
                    src="https://img.shields.io/badge/Donate-Buy%20Me%20A%20Coffee-orange.svg?style=flat-square&logo=buymeacoffee"
                    align="center"
                />
            </a></div>
<br />
